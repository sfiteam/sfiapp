## SFIApp - Festival Management Application 

<p align="center">
  <img src="logo.jpg">
</p>

---
## About

SFI - Studencki Festiwal Informatyczny - is the best student IT conference in Poland. 
The goal of the application is to help organize the festival for the organizers so that they can independently control the state using a mobile or web application


Learn more SFI at https://sfi.pl/

---

**Table of Contents** 

- [Web app project](#web-app-project)
- [Mobile app project](#mobile-app-project)
- [Our Team](#our-team)


## Web app project

Our web app is based on Angular and Firebase technology stack. 
Project was developed with kanban methodology. 
This is a screenshot from our board on GitlLab
<p align="center">
  <img src="WebAppBoard.PNG">
</p>

#### Here is use case diagram
<p align="center">
  <img src="SpaUseCase.png">
</p>

#### User Interface
<p align="center">
  <img src="Spa3.PNG">
  <img src="Spa2.PNG">
  <img src="SPAUI1Final.PNG">
  <img src="SPA5.PNG">
  <img src="Spa4Qr.PNG">
</p>

##### App site: [spaApp][]
##### Prototype: [prototypeSPA][]
##### Repository: [repositorySPA][]
##### Board: [boardSPA][]

## Mobile app project

Our mobile app is based on React Natve and Firebase technology stack. Database is shared with web application.
Project was developed with kanban methodology. 
This is a screenshot from our board on GitlLab
<p align="center">
  <img src="SFIMobileBoard.PNG">
</p>

#### Here is use case diagram
<p align="center">
  <img src="SFIMobileUSECASE.png">
</p>

#### User Interface
<p align="center">
  <img src="mobileLogin.jpg" width="300px">
  <img src="mobileHome.jpg" width="300px">
  <img src="mobileAgenda.jpg" width="300px">
</p>

##### App apk: [mobileApk][]
##### Prototype: [prototypeMobile][]
##### Repository: [repositoryMobile][]
##### Board: [boardMobile][]

## Our Team
---
##### Jan Kubierecki
##### Mateusz Hordyński
##### Dominik Kuc
##### Rafał Kiełbowicz
---

[repositorySPA]: https://gitlab.com/sfiteam/sfi-app
[boardSPA]: https://gitlab.com/sfiteam/sfi-app/boards
[prototypeSPA]: https://preview.uxpin.com/4dccf7352a956ec9ca3e968962857cb2cb09099d
[spaApp]: https://sfi.mhordynski.pl

[repositoryMobile]: https://gitlab.com/sfiteam/sfi-app-mobile
[boardMobile]: https://gitlab.com/sfiteam/sfi-app-mobile/boards
[prototypeMobile]: https://preview.uxpin.com/e98d977cab791ef9e0541d12725ec237b389fb4d
[mobileApk]: https://expo.io/artifacts/acfa4b82-abcd-4536-bc0d-d8101696e8aa